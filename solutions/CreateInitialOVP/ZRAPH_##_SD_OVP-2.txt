@EndUserText.label: 'OVP Service Definition'
define service ZRAPH_##_SB_OVP {
  expose ZRAPH_##_C_OVPFilter as OVPFilter;
  expose ZRAPH_I_OverallStatus as Status;
  expose I_Country as Country;
}